package pbparser

import java.io.File
import java.nio.charset.StandardCharsets.UTF_8

import org.scalatest.FunSpec

final class PBParserTest extends FunSpec {
  import PBParser._

  describe("PBParser") {
    it("test1") {
      assert(parseAll(syntax, """ syntax = "proto3" ; """).successful)
      assert(parseAll(intLit, """0x1""").successful)
      assert(parseAll(hexEscape, """\xff""").successful)
      assert(parseAll(strLit, """ "hoge" """).successful)
      assert(parseAll(decimalLit, """ 1 """).successful)
      assert(parseAll(octalLit, """ 01234567 """).successful)
      assert(parseAll(hexLit, """ 0xaf98""").successful)
      assert(parseAll(`import`, """import weak "hoge" ;""").successful)
      assert(parseAll(`import`, """ import public "google/protobuf/unittest_import_public_proto3.proto"; """).successful)
      assert(parseAll(primitiveType, """sint64""").successful)
    }
  }

  private[this] val ignoreErrors = List(
    "`proto3' expected but `p' found",
    "`syntax' expected but ",
    "optional "
  )

  describe("sample projects") {
    List(
      "grpc",
      "grpc-java",
      "protobuf",
      "protobuf-gradle-plugin",
      "grpc-tools",
      "googleapis"
    ).foreach{
      testSampleProtoFiles
    }
  }

  private[this] def testSampleProtoFiles(dir: String) = {

    sealed abstract class Result extends Product with Serializable
    case object OK extends Result
    case object KO extends Result
    case object Ignore extends Result

    import sbt._, sbt.Path._

    val result = (new File(dir) ** "*.proto").get.map{file =>
      val p = IO.readLines(file, UTF_8).mkString("\n")
      val r = parseAll(proto, p)
      if(r.successful){
        OK
      } else if(ignoreErrors.exists(e => r.toString.contains(e))){
        Ignore
      } else {
        println((file, r))
        KO
      }
    }.groupBy(identity).map{case (k, v) => k -> v.size}.toList.sortBy(_._1.toString)
    println((dir, result))
  }

}

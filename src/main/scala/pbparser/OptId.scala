package pbparser

import scalaz.NonEmptyList

sealed abstract class OptId extends Product with Serializable
object OptId {
  final case class Simple(value: String) extends OptId
  final case class Full(value: NonEmptyList[String]) extends OptId
}

package pbparser

sealed abstract class Type extends Product with Serializable
sealed abstract class PrimitiveType extends Type
sealed abstract class KeyType extends PrimitiveType
object Type {
  case object Double extends PrimitiveType
  case object Float extends PrimitiveType
  case object Int32 extends KeyType
  case object Int64 extends KeyType
  case object Uint32 extends KeyType
  case object Uint64 extends KeyType
  case object Sint32 extends KeyType
  case object Sint64 extends KeyType
  case object Fixed32 extends KeyType
  case object Fixed64 extends KeyType
  case object Sfixed32 extends KeyType
  case object Sfixed64 extends KeyType
  case object Bool extends KeyType
  case object String extends KeyType
  case object Bytes extends PrimitiveType
  final case class MessageType(value: List[String]) extends Type
  final case class EnumType(value: List[String]) extends Type
}


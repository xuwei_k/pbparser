package pbparser

final case class OneOfField(tpe: Type, name: String, number: Long, options: List[FieldOption])

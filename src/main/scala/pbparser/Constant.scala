package pbparser

sealed abstract class Constant extends Product with Serializable
object Constant {
  final case class Integer(value: Long) extends Constant
  final case class Float(value: Double) extends Constant
  final case class Identifier(value: String) extends Constant
  final case class StringLiteral(value: String) extends Constant
  final case class Symbol(value: String) extends Constant
}

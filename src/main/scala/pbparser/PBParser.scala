package pbparser

import pbparser.EnumBody.EnumField
import pbparser.MessageBody._

import scala.util.parsing.combinator.RegexParsers
import scalaz.{Enum => _, _}

import Types._

final case class FieldOption(name: OptionName, constant: Constant)
final case class EnumValueOption(name: OptionName, constant: Constant)

object Types {
  import PBParser.~
  type FieldOptions = List[FieldOption]
  type FieldNumber = Long
  type OptionName = OptId ~ List[String]
}

object PBParser extends RegexParsers{

  // TODO
  protected override val whiteSpace = """(\s|//.*)+""".r

  implicit class ParserOps[A](private val self: Parser[A]) extends AnyVal {
    def ++ : Parser[NonEmptyList[A]] = self.+.map{ list =>
      (list: @unchecked) match {
        case h :: t => NonEmptyList.nel(h, t)
      }
    }
    def either[B](that: Parser[B]): Parser[A \/ B] =
      self.map(\/.left) | that.map(\/.right)
  }

  def quoted[A](s: Parser[A]): Parser[A] = {
    val x = "`".r
    val y = "\"".r
    (x ~> s <~ x) | (y ~> s <~ y)
  }

  val boolLit: Parser[Boolean] = "true".r.map(_ => true) | "false".r.map(_ => false)

  val hexEscape: Parser[String] = "\\" ~> ("x" | "X") ~> "[0-9A-Fa-f]{2}".r //.map(Integer.parseInt(_, 16))
  val octEscape: Parser[String] = "\\" ~> "[0-7]{3}".r //.map(Integer.parseInt(_, 8))
  val charEscape: Parser[String] = List("a", "b", "f", "n", "r", "t", "v", "\\", "'", "\"").map{
    c => literal("\\" + c)
  }.reduceLeft(_ | _)

  // TODO
  val charValue: Parser[String] = hexEscape | octEscape | charEscape | """[^\n\\\\"]+""".r

  val strLit = quoted(charValue.+)

  val ident: Parser[String] = """[A-Za-z]{1}\w*""".r // TODO unicodeDigit
  val fullIdent: Parser[NonEmptyList[String]] = (ident ~ ("." ~> ident).*).map{
    case x ~ xs => NonEmptyList.nel(x, xs)
  }

  val messageName = ident
  val enumName = ident
  val fieldName = ident
  val oneofName = ident
  val mapName = ident
  val serviceName = ident
  val rpcName = ident

  val messageType: Parser[Type.MessageType] = {
    ".".? ~> (ident <~ ".").* ~ messageName
  }.map{case xs ~ x => Type.MessageType(xs :+ x)}

  val enumType: Parser[List[String]] = {
    ".".? ~> (ident <~ ".").* ~ enumName
  }.map{case xs ~ x => xs :+ x}

  val decimalLit: Parser[Long] = ("-?[1-9]{1}[0-9]*".r | "-?[0-9]{1}".r).map(_.toLong)
  val octalLit: Parser[Long] = "0[0-7]+".r.map(java.lang.Long.parseLong(_, 8))
  val hexLit: Parser[Long] = "0(x|X)".r ~> "[0-9A-Fa-f]+".r.map(java.lang.Long.parseLong(_, 16))
  val intLit: Parser[Long] = hexLit | decimalLit | octalLit

  val float: Parser[Double] = {
    val sign: Parser[String] = ("+" | "-").?.map(_.getOrElse(""))
    val decimals: Parser[String] = "0-9{1}[0-9]*"
    val exponent: Parser[String] = (("e" | "E") ~ sign ~ decimals).map{
      case x1 ~ x2 ~ x3 => List[String](x1, x2, x3).mkString
    }
    val f1 = (decimals ~ "." ~ decimals.?.map(_.getOrElse("")) ~ exponent).map{
      case x1 ~ x2 ~ x3 ~ x4 => List[String](x1, x2, x3, x4).mkString
    }
    val f2 = (decimals ~ exponent).map{
      case x1 ~ x2 => List[String](x1, x2).mkString
    }
    val f3 = ("." ~ decimals ~ exponent.?.map(_.getOrElse(""))).map{
      case x1 ~ x2 ~ x3 => List[String](x1, x2, x3).mkString
    }
    (f1 | f2 | f3).map(java.lang.Double.parseDouble)
  }

  val syntax: Parser[Unit] = ("syntax" ~> "=" ~> quoted("proto3") ~> ";").map(_ => ())

  val `import` : Parser[Import] = {
    val importType = ("weak".r.map(_ => ImportType.Weak) | "public".r.map(_ => ImportType.Public)).?

    ("import" ~> importType ~ strLit <~ ";").map {
      case tpe ~ name => Import(name, tpe)
    }
  }

  val emptyStatement: Parser[Unit] = ";".r.map(_ => ())

  val pack: Parser[Package] = ("package" ~> fullIdent <~ ";").map(Package)

  private[this] def tpe[A <: PrimitiveType](str: String, obj: A): Parser[A] = literal(str).map(_ => obj)

  val primitiveType: Parser[PrimitiveType] = {
    tpe("double", Type.Double) |
    tpe("float", Type.Float) |
    tpe("int32", Type.Int32) |
    tpe("int64", Type.Int64) |
    tpe("uint32", Type.Uint32) |
    tpe("uint64", Type.Uint64) |
    tpe("sint32", Type.Sint32) |
    tpe("sint64", Type.Sint64) |
    tpe("fixed32", Type.Fixed32) |
    tpe("fixed64", Type.Fixed64) |
    tpe("sfixed32", Type.Sfixed32) |
    tpe("sfixed64", Type.Sfixed64) |
    tpe("bool", Type.Bool) |
    tpe("string", Type.String) |
    tpe("bytes", Type.Bytes)
  }

  val `type`: Parser[Type] = {
    primitiveType |
    messageType |
    enumType.map(Type.EnumType)
  }

  /** https://github.com/google/protobuf/blob/v3.0.0-beta-1/src/google/protobuf/compiler/parser.cc#L1256-L1337 */
  val constant: Parser[Constant] = {
    lazy val sym: Parser[String] = "{" ~> rep1("[^{^}]+".r | sym).map(_.mkString) <~ "}"

    intLit.map(Constant.Integer) |
    float.map(Constant.Float) |
    quoted("""[\w\.]+""".r).map(Constant.StringLiteral) |
    """\w+""".r.map(Constant.Identifier) |
    sym.map(Constant.Symbol)
  }

  val optionName: Parser[OptionName] = {
    (
      ident.map(OptId.Simple) |
      ("(" ~> fullIdent.map(OptId.Full) <~ ")")
    ) ~ ("." ~> ident).*
  }

  val option: Parser[Opt] = {
    ("option" ~> optionName <~ "=") ~ constant <~ ";"
  }.map{
    case name ~ c => Opt(name, c)
  }

  val fieldOption: Parser[FieldOption] = {
    (optionName <~ "=") ~ constant
  }.map{
    case o ~ c => FieldOption(o, c)
  }

  /** non empty comma separated */
  def csv[A](parser: Parser[A]): Parser[NonEmptyList[A]] =
    (parser ~ ("," ~> parser).*).map{case x ~ xs => NonEmptyList.nel(x, xs)}

  val fieldOptions: Parser[NonEmptyList[FieldOption]] = csv(fieldOption)

  val fieldNumber = intLit

  val field = {
    (("repeated".?.map(_.isDefined) ~ `type` ~ fieldName) <~ "=") ~ (fieldNumber ~ ("[" ~> fieldOptions <~ "]").?) <~ ";"
  }.map{
    case repeated ~ tpe ~ name ~ (number ~ options) =>
      Field(repeated, tpe, name, number, options.map(_.list).getOrElse(Nil))
  }

  val enumValueOption: Parser[EnumValueOption] = {
    (optionName <~ "=") ~ constant
  }.map{
    case x ~ y => EnumValueOption(x, y)
  }

  val enumField = {
    (ident <~ "=") ~ intLit ~ ("[" ~> csv(enumValueOption) <~ "]").? <~ ";"
  }.map{
    case x ~ y ~ z => EnumField(x, y, z.map(_.list).getOrElse(Nil))
  }

  val enumBody: Parser[List[EnumBody]] =
    "{" ~> ((option | enumField).map(Option(_)) | emptyStatement.map(_ => None)).++.map(_.list.flatten) <~ "}"

  val enum: Parser[Enum] = ("enum" ~> enumName ~ enumBody).map{
    case name ~ body => MessageBody.Enum(name, body)
  }

  val keyType: Parser[KeyType] = {
    tpe("int32", Type.Int32) |
    tpe("int64", Type.Int64) |
    tpe("uint32", Type.Uint32) |
    tpe("uint64", Type.Uint64) |
    tpe("sint32", Type.Sint32) |
    tpe("sint64", Type.Sint64) |
    tpe("fixed32", Type.Fixed32) |
    tpe("fixed64", Type.Fixed64) |
    tpe("sfixed32", Type.Sfixed32) |
    tpe("sfixed64", Type.Sfixed64) |
    tpe("bool", Type.Bool) |
    tpe("string", Type.String)
  }

  val mapField: Parser[MapField] = {
    (("map" ~> "<" ~> keyType <~ ",") ~ `type` <~ ">") ~ (mapName <~ "=") ~ fieldNumber <~ ";"
  }.map{
    case key ~ value ~ name ~ number =>
      MapField(key, value, name, number)
  }

  val range: Parser[NumRange] = {
    (intLit ~ ("to" ~> (intLit.map(Option(_)) | "max".r.map(_ => None))).?).map{
      case from ~ Some(Some(to)) => NumRange.To(from, to)
      case from ~ Some(None) => NumRange.ToMax(from)
      case value ~ None => NumRange.Single(value)
    }
  }

  val reserved: Parser[Reserved] = {
    "reserved" ~> (csv(range).map(Ranges) | csv(quoted(fieldName)).map(FieldNames)) <~ ";"
  }

  val oneOfField: Parser[OneOfField] = {
    ((`type` ~ fieldName <~ "=") ~ fieldNumber ~ ("[" ~> fieldOptions <~ "]").?) <~ ";"
  }.map{
    case tpe ~ name ~ number ~ options =>
      OneOfField(tpe, name, number, options.map(_.list).getOrElse(Nil))
  }

  val oneOf: Parser[OneOf] = {
    ("oneof" ~> oneofName) ~ ("{" ~> orEmpty(oneOfField) <~ "}")
  }.map{
    case name ~ fields => OneOf(name, fields)
  }

  lazy val messageBody: Parser[List[MessageBody]] = {
    "{" ~> orEmpty(field | enum | message | option | oneOf | mapField | reserved) <~ "}"
  }

  lazy val message: Parser[Message] = {
    "message" ~> messageName ~ messageBody
  }.map{
    case name ~ body => Message(name, body)
  }

  private def orEmpty[A](parser: Parser[A]): Parser[List[A]] = {
    (parser.map(Option(_)) | emptyStatement.map(_ => None)).*.map(_.flatten)
  }

  val rpc: Parser[RPC] = {
    val stream = "stream".?.map(_.isDefined)

    ("rpc" ~> rpcName) ~
    ("(" ~> stream ~ messageType <~ ")") ~
    (("returns" ~> "(") ~> stream ~ messageType <~ ")") ~
    ("{" ~> orEmpty(option) <~ "}").either(";")
  }.map{
    case name ~ (s1 ~ req) ~ (s2 ~ res) ~ options =>
      RPC(name, s1, req, s2, res, options.swap.getOrElse(Nil))
  }

  val service: Parser[Service] = {
    ("service" ~> serviceName) ~ ("{" ~> orEmpty(option | rpc) <~ "}")
  }.map{
    case name ~ body => Service(name, body)
  }

  val topLevelDef: Parser[TopLevelDef] = message | enum | service

  val proto: Parser[Proto] = syntax ~> {
    orEmpty(`import` | pack | option | topLevelDef).map(Proto)
  }

}




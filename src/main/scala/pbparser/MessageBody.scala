package pbparser

import pbparser.MessageBody.Opt
import pbparser.Type.MessageType
import pbparser.Types._

import scalaz.NonEmptyList


sealed abstract class MessageBody extends Product with Serializable

sealed trait Reserved extends MessageBody

object MessageBody {
  final case class Field(repeated: Boolean, tpe: Type, name: String, number: FieldNumber, options: FieldOptions) extends MessageBody
  final case class Enum(name: String, body: List[EnumBody]) extends MessageBody with TopLevelDef
  final case class Message(name: String, body: List[MessageBody]) extends MessageBody with TopLevelDef
  final case class Opt(name: OptionName, constant: Constant) extends MessageBody with EnumBody with ProtoBody with ServiceBody
  final case class OneOf(name: String, fields: List[OneOfField]) extends MessageBody
  final case class MapField(key: KeyType, value: Type, name: String, number: Long) extends MessageBody
  final case class FieldNames(value: NonEmptyList[String]) extends Reserved
  final case class Ranges(value: NonEmptyList[NumRange]) extends Reserved
}

final case class Service(name: String, body: List[ServiceBody]) extends TopLevelDef

sealed trait EnumBody extends Product with Serializable
object EnumBody{
  val Opt = MessageBody.Opt
  final case class EnumField(name: String, number: Long, options: List[EnumValueOption]) extends EnumBody
}

sealed trait TopLevelDef extends ProtoBody

sealed trait ProtoBody extends Product with Serializable

final case class Import(name: List[String], tpe: Option[ImportType]) extends ProtoBody
final case class Package(value: NonEmptyList[String]) extends ProtoBody


sealed trait ServiceBody extends Product with Serializable

final case class RPC(
  name: String,
  requestStream: Boolean,
  requestType: MessageType,
  responseStream: Boolean,
  responseType: MessageType,
  options: List[Opt]
) extends ServiceBody

sealed abstract class NumRange extends Product with Serializable
object NumRange {
  final case class Single(value: Long) extends NumRange
  final case class To(from: Long, to: Long) extends NumRange
  final case class ToMax(from: Long) extends NumRange
}

package pbparser

sealed abstract class ImportType extends Product with Serializable

object ImportType {
  case object Weak extends ImportType
  case object Public extends ImportType
}
